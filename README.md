# Demo .Net GitLab CI
A demo .Net project analyzed with GitLab CI and SonarQube .Net scanner integration
Most of the interesting part is in the [Gitlab-CI YAML pipeline](https://gitlab.com/okorach/demo-gitlabci-dotnet/-/blob/master/.gitlab-ci.yml)